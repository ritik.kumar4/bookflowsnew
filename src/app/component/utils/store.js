"use client";

import React, { createContext, useState } from "react";

export const BookContext = createContext();

export function SlotBookingProvider({ children }) {
  const currentDate = new Date();
  const [selectedYear, setSelectedYear] = useState(currentDate.getFullYear());
  return <BookContext.Provider value={""}>{children}</BookContext.Provider>;
}
