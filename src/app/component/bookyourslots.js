"use client";
import React from "react";
import { useState } from "react";
import FillYourInfo from "./fillyourinfo";
import { useEffect } from "react";
import DateCarousel from "./datacarousel";
import TimeCarousel from "./slotsCarousel";
import {
  format,
  addMonths,
  subMonths,
  startOfMonth,
  endOfMonth,
  eachDayOfInterval,
} from "date-fns";
import { LocalizationProvider, StaticDatePicker } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import Link from "@mui/material/Link";
import axios from "axios";
import { BASE_ASSET_URL } from "../../../utils";
import BooingConfirm from "./bookingconfirm";

// const BookYourSlots = () =>{
//   return(
//     <BooingConfirm/>
//   )
// }
const BookYourSlots = ({ id }) => {
  const [selectedDate, setSelectedDate] = useState(null);
  const [selectedTab, setSelectedTab] = useState("morning");
  const currentDate = new Date();
  const [selectedMonth, setSelectedMonth] = useState(
    currentDate.getMonth() + 1
  );
  const [selectedYear, setSelectedYear] = useState(currentDate.getFullYear());
  const [showAppointment, setShowAppointment] = useState(false);
  const [CurrentComp, setCurrentComp] = useState(true);
  const [IsMobile, setIsMobile] = useState(false);
  const [value, setValue] = useState(null);
  const [openDatePicker, setOpenDatePicker] = useState(false);
  const [openDatePicker2, setOpenDatePicker2] = useState(false);
  const [selectedDay, setSelectedDay] = useState("Monday");
  const [morningtimingsData, setMorningTimingsData] = useState([]);
  const [aftertimingsData, setAfternoonTimingsData] = useState([]);
  const [eveningtimingsData, setEveningTimingsData] = useState([]);
  const [date, setDate] = useState();
  const [slot, setSlots] = useState();
  const [number, setNumber] = useState();
  const [AdminId, setAdminId] = useState();
  const [filteredSlotsData, setFilteredSlotsData] = useState();
  const [bookedSlot, setBookedSlot] = useState({
    morning: [],
    afternoon: [],
    evening: [],
  });

  const handleTimeSlotClick = (time) => {
    const updatedBookedSlot = {
      morning: selectedTab === "morning" ? [time] : [],
      afternoon: selectedTab === "afternoon" ? [time] : [],
      evening: selectedTab === "evening" ? [time] : [],
    };
    setSlots(time);
    setBookedSlot(updatedBookedSlot);
  };

  const getFullDayName = (abbreviatedDay) => {
    const abbreviatedDays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
    const fullDays = [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday",
    ];

    const index = abbreviatedDays.indexOf(abbreviatedDay);
    if (index !== -1) {
      return fullDays[index];
    } else {
      return null;
    }
  };

  const getday = (day) => {
    const fullDayName = getFullDayName(day);
    if (fullDayName) {
      console.log("dayyyynaaame:--->" + fullDayName);
      setSelectedDay(fullDayName);
      getslots();
    } else {
      console.error("Invalid abbreviated day name:", day);
    }
  };

  useEffect(() => {
    localStorage.setItem("selectedday", selectedDay);
  }, [selectedDay]);

  console.log("---->" + selectedDay);
  const getDate = (dateitem) => {
    // localStorage.setItem("selectedDate", dateitem);
    setDate(dateitem);
    console.log("dateitem-------->" + dateitem);
  };

  const getnumber = async (Number) => {
    await postData(Number);
  };
  console.log("Number------>" + number);

  console.log("slectedslotssss-->" + slot);
  if (typeof window !== "undefined") {
    localStorage.setItem("selectedSlots", slot);
    localStorage.setItem("selectedDate", date);
    localStorage.setItem("AdminId", id);
  }
  const getslots = async () => {
    try {
      const appointmentsResponse = await axios.get(
        `${BASE_ASSET_URL}/appointment/admin/${id}`
      );
      const appointmentDetailsResponse = await axios.get(
        `${BASE_ASSET_URL}/appointment-details/admin/${id}`
      );

      const appointmentsData = appointmentsResponse.data;
      const appointmentDetailsData = appointmentDetailsResponse.data;

      console.log("Appointments Data:---->", appointmentsData);
      // console.log('Appointment Details Data:', appointmentDetailsData);

      const filteredSlots = {};

      appointmentsData.forEach((appointment) => {
        const dayName = appointment.day;
        const filteredDaySlots = { morning: [], afternoon: [], evening: [] };

        // Find all booked slots for the current day
        const bookedSlotsForDay = appointmentDetailsData
          .filter((detail) => detail.day === dayName)
          .map((detail) => ({
            status: detail.status,
            bookedSlot: detail.bookedSlot,
          }));

        // Merge all booked slots into a single array for each time slot period (morning, afternoon, evening)
        const allBookedSlots = {
          morning: bookedSlotsForDay.flatMap(
            (detail) => detail.bookedSlot.morning
          ),
          afternoon: bookedSlotsForDay.flatMap(
            (detail) => detail.bookedSlot.afternoon
          ),
          evening: bookedSlotsForDay.flatMap(
            (detail) => detail.bookedSlot.evening
          ),
        };

        // Filter time slots based on booked slots
        // Filter morning slots
        appointment.timeSlots.morning.forEach((slot) => {
          if (!allBookedSlots.morning.includes(slot)) {
            filteredDaySlots.morning.push(slot);
          } else {
            // Check status of booked slot
            const bookedSlotStatus = bookedSlotsForDay.find((detail) =>
              detail.bookedSlot.morning.includes(slot)
            )?.status;
            if (
              bookedSlotStatus === "Rescheduled" ||
              bookedSlotStatus === "Cancelled"
            ) {
              filteredDaySlots.morning.push(slot);
            }
          }
        });

        // Filter afternoon slots
        appointment.timeSlots.afternoon.forEach((slot) => {
          if (!allBookedSlots.afternoon.includes(slot)) {
            filteredDaySlots.afternoon.push(slot);
          } else {
            // Check status of booked slot
            const bookedSlotStatus = bookedSlotsForDay.find((detail) =>
              detail.bookedSlot.afternoon.includes(slot)
            )?.status;
            if (
              bookedSlotStatus === "Rescheduled" ||
              bookedSlotStatus === "Cancelled"
            ) {
              filteredDaySlots.afternoon.push(slot);
            }
          }
        });

        // Filter evening slots
        appointment.timeSlots.evening.forEach((slot) => {
          if (!allBookedSlots.evening.includes(slot)) {
            filteredDaySlots.evening.push(slot);
          } else {
            // Check status of booked slot
            const bookedSlotStatus = bookedSlotsForDay.find((detail) =>
              detail.bookedSlot.evening.includes(slot)
            )?.status;
            if (
              bookedSlotStatus === "Rescheduled" ||
              bookedSlotStatus === "Cancelled"
            ) {
              filteredDaySlots.evening.push(slot);
            }
          }
        });

        filteredSlots[dayName] = filteredDaySlots;
      });

      console.log("Filtered Slots:", filteredSlots);

      // Set the filtered time slots in the state based on selectedDay
      console.log("selectedDay------>:", selectedDay);
      const selectedDaySlots = filteredSlots[selectedDay];
      console.log("selectedDaySlots:", selectedDaySlots);
      if (selectedDaySlots) {
        setMorningTimingsData(selectedDaySlots.morning);
        setAfternoonTimingsData(selectedDaySlots.afternoon);
        setEveningTimingsData(selectedDaySlots.evening);
      } else {
        setMorningTimingsData([]);
        setAfternoonTimingsData([]);
        setEveningTimingsData([]);
      }

      setFilteredSlotsData(filteredSlots);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  useEffect(() => {
    const retrievedAdminId = localStorage.getItem("AdminId");
    if (retrievedAdminId) {
      setAdminId(retrievedAdminId);
    }
  }, []);
  // let AdminId = localStorage.getItem("AdminId");
  const postData = async (phoneNumber) => {
    try {
      const dataToSend = {
        adminId: AdminId,
        serialNo: "12345678",
        day: selectedDay,
        purposeOfVisit: "",
        status: "Active",
        date: date,
        bookedSlot: bookedSlot,
        phoneNumber: phoneNumber,
      };

      const response = await axios.post(
        `${BASE_ASSET_URL}/appointment-details`,
        dataToSend
      );
      console.log("Response:", response.data);
      localStorage.setItem("appointmentDetailId", response.data._id);
    } catch (error) {
      console.error("Error:", error);
    }
  };

  //
  useEffect(() => {
    getslots();
    // postData();
  }, [date,selectedDay]);

  const handleLinkClick = () => {
    setOpenDatePicker(true);
  };

  const handleLinkClick2 = () => {
    setOpenDatePicker2(true);
  };

  const handleDatePickerClose = () => {
    setOpenDatePicker(false);
  };

  const handleDatePickerClose2 = () => {
    setOpenDatePicker2(false);
  };

  const handleMonthPickerOpen = () => {
    setIsMonthPickerOpen(true);
  };

  const handleMonthPickerClose = () => {
    setIsMonthPickerOpen(false);
  };

  const handleMonthChange = (increment) => {
    const newDate = new Date(selectedYear, selectedMonth - 1 + increment, 1);
    setSelectedMonth(newDate.getMonth() + 1);
    setSelectedYear(newDate.getFullYear());
  };
  const handleBookAppointmentClick = () => {
    setShowAppointment(true);
    setCurrentComp(false);
  };

  const handleTabClick = (tab) => {
    setSelectedTab(tab);
  };

  const handleMonthPickerChange = (newDate) => {
    setSelectedMonth(new Date(newDate).getMonth() + 1);
    handleDatePickerClose();

    console.log("Selected Date:", new Date(newDate).getMonth());
  };

  const handleYearPickerChange = (newDate) => {
    setSelectedYear(new Date(newDate).getFullYear());
    handleDatePickerClose2();
    console.log("Selected Date:", new Date(newDate).getFullYear());
  };

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <>
        {CurrentComp && (
          <div className="book_demo2">
            <div className="opaque_slot">
              <div className="container">
                <div className="row">
                  <div className="col-md-12">
                    <div className="top_book_slot">
                      <div className="row">
                        <div className="col-md-8 top_slot_left slideInLeft">
                          <h4>Book Your Slot</h4>
                        </div>
                        <div className="col-md-4">
                          <div className="row  slideInRight">
                            <div className="col-md-6 col-xs-6 december">
                              <h4>
                                <div>
                                  <a
                                    href="#"
                                    onClick={() => handleMonthChange(-1)}
                                  >
                                    <img
                                      src="/images/year_arrow_left.png"
                                      className="img-responsive"
                                      alt=""
                                    />{" "}
                                  </a>{" "}
                                  <Link
                                    style={{
                                      color: "white",
                                      cursor: "pointer",
                                    }}
                                    onClick={handleLinkClick}
                                  >
                                    {format(
                                      new Date(selectedYear, selectedMonth - 1),
                                      "MMMM"
                                    )}
                                  </Link>
                                  <a
                                    href="#"
                                    onClick={() => handleMonthChange(1)}
                                  >
                                    <img
                                      src="images/year_arrow_right.png"
                                      className="img-responsive"
                                      alt=""
                                    />{" "}
                                  </a>
                                  {openDatePicker && (
                                    <div
                                      style={{
                                        zIndex: 999,
                                        position: "absolute",
                                      }}
                                    >
                                      {" "}
                                      <StaticDatePicker
                                        label={'"Year"'}
                                        openTo="Year"
                                        views={["month"]}
                                        sx={{ color: "#000" }}
                                        open={openDatePicker}
                                        onClose={handleDatePickerClose}
                                        onChange={handleMonthPickerChange}
                                      />
                                    </div>
                                  )}
                                  {openDatePicker2 && (
                                    <div
                                      style={{
                                        zIndex: 999,
                                        position: "absolute",
                                      }}
                                    >
                                      {" "}
                                      <StaticDatePicker
                                        label={'"Year"'}
                                        openTo="Year"
                                        views={["year"]}
                                        sx={{ color: "#000" }}
                                        open={openDatePicker2}
                                        onClose={handleDatePickerClose2}
                                        onChange={handleYearPickerChange}
                                      />
                                    </div>
                                  )}
                                </div>
                              </h4>
                            </div>
                            <div className="col-md-5 col-xs-6 december">
                              <h4>
                                <a
                                  href="#"
                                  onClick={() => handleMonthChange(-12)}
                                >
                                  <img
                                    src="images/year_arrow_left.png"
                                    className="img-responsive"
                                    alt=""
                                  />{" "}
                                </a>{" "}
                                <Link
                                  style={{ color: "white", cursor: "pointer" }}
                                  onClick={handleLinkClick2}
                                >
                                  {format(
                                    new Date(selectedYear, selectedMonth - 1),
                                    "yyyy"
                                  )}{" "}
                                </Link>
                                <a
                                  href="#"
                                  onClick={() => handleMonthChange(12)}
                                >
                                  <img
                                    src="images/year_arrow_right.png"
                                    className="img-responsive"
                                    alt=""
                                  />{" "}
                                </a>
                              </h4>
                            </div>

                            <div className="clearfix" />
                          </div>
                          <div className="clearfix" />
                        </div>
                        <div className="clearfix" />
                      </div>
                      <div className="clearfix" />
                    </div>
                  </div>
                  <div className="clearfix" />

                  <DateCarousel
                    selectedMonth={selectedMonth}
                    selectedYear={selectedYear}
                    getday={getday}
                    getDate={getDate}
                  ></DateCarousel>
                </div>
                <div className="clearfix" />
              </div>
            </div>
            <div className="book_demo book_slot">
              <div className="container">
                <div className="row">
                  <div className="clearfix" />
                  <div className="col-md-12 morning info_bottom2">
                    <ul className="nav nav-tabs">
                      <li className={selectedTab === "morning" ? "active" : ""}>
                        <a
                          data-toggle="tab"
                          href="#morning"
                          onClick={() => handleTabClick("morning")}
                        >
                          Morning Slots
                        </a>
                      </li>
                      <li
                        className={selectedTab === "afternoon" ? "active" : ""}
                      >
                        <a
                          data-toggle="tab"
                          href="#afternoon"
                          onClick={() => handleTabClick("afternoon")}
                        >
                          Afternoon Slots
                        </a>
                      </li>
                      <li className={selectedTab === "evening" ? "active" : ""}>
                        <a
                          data-toggle="tab"
                          href="#evening"
                          onClick={() => handleTabClick("evening")}
                        >
                          Evening Slots
                        </a>
                      </li>
                    </ul>
                    <div className="tab-content">
                      <div
                        id="morning"
                        className={`tab-pane fade ${
                          selectedTab === "morning" ? "in active" : ""
                        }`}
                      >
                        <ul className="timings_list">
                          {/* {IsMobile ? (
      <TimeCarousel></TimeCarousel>
    ) : ( */}
                          <>
                            {morningtimingsData.map((time, index) => (
                              <li key={index} className="zoomIn">
                                <a
                                  href="#"
                                  className={index === 1 ? "active" : ""}
                                  onClick={() => handleTimeSlotClick(time)}
                                >
                                  {time}
                                </a>
                              </li>
                            ))}
                          </>
                          {/* )} */}
                        </ul>
                        <div className="text-center b_a_btn">
                          <a
                            className="btn btn_new2"
                            onClick={handleBookAppointmentClick}
                          >
                            Book Appointment
                          </a>
                        </div>
                      </div>
                      <div
                        id="afternoon"
                        className={`tab-pane fade ${
                          selectedTab === "afternoon" ? "in active" : ""
                        }`}
                      >
                        <ul className="timings_list">
                          {/* {IsMobile ? (
      <TimeCarousel></TimeCarousel>
    ) : ( */}
                          <>
                            {aftertimingsData.map((time, index) => (
                              <li key={index} className="zoomIn">
                                <a
                                  href="#"
                                  className={index === 1 ? "active" : ""}
                                  onClick={() => handleTimeSlotClick(time)}
                                >
                                  {time}
                                </a>
                              </li>
                            ))}
                          </>
                          {/* )} */}
                        </ul>
                        <div className="text-center b_a_btn">
                          <a
                            className="btn btn_new2"
                            onClick={handleBookAppointmentClick}
                          >
                            Book Appointment
                          </a>
                        </div>
                      </div>
                      <div
                        id="evening"
                        className={`tab-pane fade ${
                          selectedTab === "evening" ? "in active" : ""
                        }`}
                      >
                        <ul className="timings_list">
                          {/* {IsMobile ? (
      <TimeCarousel></TimeCarousel>
    ) : ( */}
                          <>
                            {eveningtimingsData.map((time, index) => (
                              <li key={index} className="zoomIn">
                                <a
                                  href="#"
                                  className={index === 1 ? "active" : ""}
                                  onClick={() => handleTimeSlotClick(time)}
                                >
                                  {time}
                                </a>
                              </li>
                            ))}
                          </>
                          {/* )} */}
                        </ul>
                        <div className="text-center b_a_btn">
                          <a
                            className="btn btn_new2"
                            onClick={handleBookAppointmentClick}
                          >
                            Book Appointment
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="clearfix" />
                <div className="row">
                  <div className="col-md-12"></div>
                  <div className="clearfix" />
                </div>
                <div className="clearfix" />
              </div>
            </div>
          </div>
        )}
        {showAppointment && <FillYourInfo getnumber={getnumber} />}
      </>
    </LocalizationProvider>
  );
};

export default BookYourSlots;
