// components/GoogleCalendarSync.js
import React, { useEffect } from 'react';
import { useGoogleLogin } from 'react-google-login';
import { gapi } from 'gapi-script';

const clientId = '974755695102-1qdpcaj6g5okg2v1bekq9vg4koddscj5.apps.googleusercontent.com';

const GoogleCalendarSync = (props) => {
  const eventData = {
    summary: 'Vet Appointment',
    description: 'Booked with Bookflows',
    startDateTime: '2024-08-17T10:00:00',
    endDateTime: '2024-08-17T11:00:00',
  };

  useEffect(() => {
    const initClient = () => {
      gapi.load('client:auth2', () => {
        gapi.client.init({
          clientId: clientId,
          scope: 'https://www.googleapis.com/auth/calendar',
        }).then(() => {
          console.log("GAPI client initialized.");
        }).catch((error) => {
          console.error("Error initializing GAPI client", error);
        });
      });
    };

    if (!gapi.client) {
      initClient();
    }
  }, []);


  const responseGoogle = (response) => {
    console.log(response);
    if (response.accessToken) {
      addEvent(response.accessToken, eventData);
    }
  };

  const { signIn } = useGoogleLogin({
    onSuccess: responseGoogle,
    onFailure: responseGoogle,
    clientId: clientId,
    isSignedIn: false,
    accessType: 'offline',
    scope: 'https://www.googleapis.com/auth/calendar',
  });


  const addEvent = (accessToken, eventData) => {
    const { summary, description, startDateTime, endDateTime } = eventData;

    gapi.client.setToken({ access_token: accessToken });
    gapi.client.load('calendar', 'v3', () => {
      const event = {
        summary: summary,
        description: description,
        start: {
          dateTime: new Date(startDateTime).toISOString(),
          timeZone: 'America/Los_Angeles',
        },
        end: {
          dateTime: new Date(endDateTime).toISOString(),
          timeZone: 'America/Los_Angeles',
        },
      };

      gapi.client.calendar.events
        .insert({
          calendarId: 'primary',
          resource: event,
        })
        .then((response) => {
          console.log('Event created: ', response);
          alert('Event created successfully!');
        })
        .catch((err) => {
          console.error('Error creating event: ', err);
          alert('Error creating event.');
        });
    });
  };

  const handleButtonClick = () => {
    signIn();
  };
  return (
    <div>
      <button style={{marginBottom:10}} className="btn  btn_new22" onClick={handleButtonClick}><img style={{width:'30px'}} src='https://cdn1.iconfinder.com/data/icons/google-s-logo/150/Google_Icons-03-512.png'/> Sync with Google Calendar</button>
    </div>
  );
};

export default GoogleCalendarSync;
