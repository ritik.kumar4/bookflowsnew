import React, { useRef, useState, useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {
  format,
  startOfMonth,
  endOfMonth,
  eachDayOfInterval,
  getDate,
  isSameDay,
} from "date-fns";

const DateCarousel = ({ selectedMonth, selectedYear, getday, getDate }) => {
  const [dateItems, setDateItems] = useState([]);
  const [selectedDateIndex, setSelectedDateIndex] = useState(null);
  const today = new Date();

  const sliderRef = useRef(null);

  useEffect(() => {
    const generateDateItems = () => {
      const startDate = startOfMonth(new Date(selectedYear, selectedMonth - 1));
      let endDate;

      // Check if the selected month is December
      if (selectedMonth === 12) {
        // If December, set the end date to the last day of the year
        endDate = endOfMonth(new Date(selectedYear, 11, 31)); // December is month 11 (0-indexed)
      } else {
        // For other months, set the end date to the end of the month
        endDate = endOfMonth(startDate);
      }

      const days = eachDayOfInterval({ start: startDate, end: endDate });

      const formattedDates = days.map((day) => ({
        date: format(day, "d"),
        month: format(day, "MMM"),
        separator: "|",
        day: format(day, "EEE"),
        isToday: isSameDay(day, today),
      }));

      setDateItems(formattedDates);

      // Find today's date in the array of formatted dates
      const todayIndex = formattedDates.findIndex((date) =>
        isSameDay(
          new Date(),
          new Date(selectedYear, selectedMonth - 1, parseInt(date.date, 10))
        )
      );

      // Set the selectedDateIndex to today's index
     if (todayIndex !== -1 && formattedDates[todayIndex].isToday) {
        getday(formattedDates[todayIndex].day);
        const date =
          formattedDates[todayIndex].date +
          " " +
          formattedDates[todayIndex].month +
          " " +
          selectedYear;
        getDate(date);
      }

      // Adjust carousel position to today's date
      if (sliderRef.current && todayIndex !== -1) {
        sliderRef.current.slickGoTo(todayIndex);
      }
    };

    generateDateItems();
  }, [selectedMonth, selectedYear]);

  const settings = {
    infinite: false,
    slidesToShow: 9,
    slidesToScroll: 1,
    autoplay: false,
    speed: 200,
    dots: false,
    arrows: true,
    swipeToSlide: true,
    touchMove: true,
    responsive: [
      {
        breakpoint: 481,
        settings: {
          slidesToShow: 4,
          arrows: false,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 6,
          arrows: false,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 7,
          arrows: false,
        },
      },
    ],
  };

  const handlePrev = () => {
    if (sliderRef.current) {
      sliderRef.current.slickPrev();
    }
  };

  const handleNext = () => {
    if (sliderRef.current) {
      sliderRef.current.slickNext();
    }
  };

  const handleDateBoxClick = (index, item) => {
    setSelectedDateIndex(index);
    getday(item.day);
    const date = item.date + " " + item.month + " " + selectedYear;
    getDate(date);
  };

  return (
    <div>
      <div className="customNavigation5">
        <a className="btn prev5" onClick={handlePrev}>
          <img src="images/l_o.png" alt="Left" />
        </a>
        <a className="btn next5" onClick={handleNext}>
          <img src="images/r_o.png" alt="Right" />
        </a>
      </div>

      <Slider ref={sliderRef} {...settings}>
        {dateItems.map((item, index) => (
          <div key={index} className={`item`}>
            <div
              className={`date_box ${
                selectedDateIndex === index ? "active" : ""
              }`}
              style={{ flex: "0 0 auto" }}
              onClick={() => handleDateBoxClick(index, item)}
            >
              <div className="date_top">
                <h3 style={{ color: "white" }}>{item.date}</h3>
              </div>
              <div className="date_bottom">
                <h5 style={{ color: "white" }}>{item.month}</h5>
                <h3 style={{ color: "white" }}>{item.separator}</h3>
                <p style={{ color: "white" }}>{item.day}</p>
              </div>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default DateCarousel;
