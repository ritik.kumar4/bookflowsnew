"use client"
import React, { useState, useEffect } from "react";
import BookYourSlots from "./bookyourslots";
import axios from "axios";
import { BASE_ASSET_URL } from "../../../utils";
import Draggable from 'react-draggable'; // Import Draggable component
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

// Define DraggableDialog component
function DraggableDialog({ open, handleClose, handleConfirmation, confirmationMessage }) {
  return (
    <Draggable cancel={".MuiDialogContent-root"}>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle style={{ cursor: 'move' }}>Confirmation</DialogTitle>
        <DialogContent>
          <DialogContentText>{confirmationMessage}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose}>Cancel</Button>
          <Button onClick={() => handleConfirmation(true)}>Yes</Button>
        </DialogActions>
      </Dialog>
    </Draggable>
  );
}

const BookingConfirm = () => {
  const [showAppointment, setShowAppointment] = useState(false);
  const [CurrentComp, setCurrentComp] = useState(true);
  const [name, setName] = useState("");
  const [data, setData] = useState({
    name: "",
    emailId: "",
    contactNumber: "",
    address: "",
    logo: ""
  });
  const [isConfirmationOpen, setIsConfirmationOpen] = useState(false);
  const [confirmationAction, setConfirmationAction] = useState("");
  
  const tempslot = localStorage.getItem('selectedSlots');
  const selectedSlots = tempslot ? tempslot : "";
  const tempdate = localStorage.getItem('selectedDate');
  const selectedDate = tempdate ? tempdate : "";
  const tempday = localStorage.getItem('selectedday');
  const selectedDay = tempday ? tempday : "";

  const handleBookAppointmentClick = () => {
    setConfirmationAction("Cancelled");
    setIsConfirmationOpen(true);
  };

  const handleBookAppointmentClick2 = () => {
    setConfirmationAction("Rescheduled");
    setIsConfirmationOpen(true);
  };

  const postData = async (bookingStatus) => {
    const appointmentId = localStorage.getItem("appointmentDetailId");
    try {
      const payload = { status: bookingStatus };
      await axios.patch(
        `${BASE_ASSET_URL}/appointment-details/${appointmentId}`,
        payload
      );
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const getDoctor = async () => {
    try {
      const response = await axios.get(`${BASE_ASSET_URL}/businessinfo`);
      const doctorData = response.data[0];
      setData({
        name: doctorData.name || "",
        emailId: doctorData.emailId || "",
        contactNumber: doctorData.contactNumber || "",
        address: doctorData.address || "",
        logo: doctorData.logo || ""
      });
      setName(doctorData.name);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    getDoctor();
  }, []);

  const handleConfirmation = async (confirmed) => {
    if (confirmed) {
      await postData(confirmationAction);
      setShowAppointment(true);
    }
    setIsConfirmationOpen(false);
  };

  return (
    <>
      <DraggableDialog
        open={isConfirmationOpen}
        handleClose={() => setIsConfirmationOpen(false)}
        handleConfirmation={handleConfirmation}
        confirmationMessage={`Are you sure you want to ${confirmationAction.toLowerCase()}?`}
      />
      {CurrentComp && (
        <div className="colony">
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-md-offset-1">
                <div className="confirmed">
                  <div className="row">
                    <div className="col-md-12 tick_top zoomIn">
                      <img src="images/person.png" className="img-responsive center-block" alt="" />
                    </div>
                    <div className="col-md-12">
                      <h2>{name}</h2>
                    </div>
                    <div className="col-md-12 bookingcnf">
                      <h2><span><i className="fa fa-phone" style={{ fontSize: '16px', color: 'white', marginRight: '2px' }}></i></span>+91 {data.contactNumber}</h2>
                      <h2><span><i className="fa fa-envelope" style={{ fontSize: '16px', color: 'white', marginRight: '2px' }}></i></span>{data.emailId}</h2>
                    </div>
                    <div className="bookingcnf2">
                      <h2><span><i className="fa fa-map-marker" style={{ fontSize: '16px', color: 'white', marginRight: '2px' }}></i></span> {data.address}</h2>
                    </div>
                    <div className="clearfix" />
                  </div>
                  <div className="clearfix" />
                  <div className="divider"></div>
                  <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginBottom: '45px' }}>
                    <a href="#" className="btn btn_new21">{selectedSlots}</a>
                    <a href="#" className="btn btn_new21">{selectedDate}</a>
                    <a href="#" className="btn btn_new21">{selectedDay}</a>
                  </div>
                  <div className="row">
                    <div className="clearfix" />
                    <div className="text-center b_a_btn">
                      <Button variant="outlined" onClick={handleBookAppointmentClick2}>Re-Schedule</Button>
                      <Button variant="outlined" onClick={handleBookAppointmentClick}>Cancel</Button>
                    </div>
                  </div>
                  <div className="clearfix" />
                </div>
              </div>
              <div className="clearfix" />
            </div>
            <div className="clearfix" />
          </div>
        </div>
      )}
      {showAppointment && <BookYourSlots />}
    </>
  );
}

export default BookingConfirm;
