"use client";
import { useState } from "react";
import BookYourSlots from "./bookyourslots";
import { useEffect } from "react";
import axios from "axios";
import { BASE_ASSET_URL } from "../../../utils";
import Draggable from "react-draggable"; // Import Draggable component
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import SendRoundedIcon from "@mui/icons-material/SendRounded";
import GoogleCalendarSync from "./GoogleCalendarSync";

function DraggableDialog({
  open,
  handleClose,
  handleConfirmation,
  confirmationMessage,
}) {
  return (
    <Draggable cancel={".MuiDialogContent-root"}>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            minWidth: "400px",
            maxWidth: "800px",
            transition: "opacity 0.3s ease-in-out, transform 0.3s ease-in-out",
          },
        }}
      >
        <DialogTitle style={{ cursor: "move" }}>Confirmation</DialogTitle>
        <DialogContent>
          <DialogContentText style={{ fontSize: "16px" }}>
            {confirmationMessage}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={handleClose}
            style={{ minWidth: "120px", marginRight: "10px", fontSize: "16px" }}
          >
            Cancel
          </Button>
          <Button
            onClick={() => handleConfirmation(true)}
            style={{ minWidth: "120px", fontSize: "16px" }}
          >
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </Draggable>
  );
}

const BooingConfirm = () => {
  const [showAppointment, setShowAppointment] = useState(false);
  const [CurrentComp, setCurrentComp] = useState(true);
  const [name, setname] = useState();
  const [bookingStatus, SetBookingStatus] = useState();
  const [isConfirmationOpen, setIsConfirmationOpen] = useState(false);
  const [confirmationAction, setConfirmationAction] = useState("");
  const [status, SetStatus] = useState("Active");
  const [pov, setpov] = useState();
  const [data, setdata] = useState({
    name: "",
    emailId: "",
    contactNumber: "",
    address: "",
    logo: "",
  });

  let tempslot = localStorage.getItem("selectedSlots");
  const selectedSlots = tempslot ? tempslot : "";
  let tempdate = localStorage.getItem("selectedDate");
  const selectedDate = tempdate ? tempdate : "";
  let tempday = localStorage.getItem("selectedday");
  const selectedDay = tempday ? tempday : "";

  const handleBookAppointmentClick = () => {
    SetStatus("Cancelled");
    setConfirmationAction("Cancel Appointment");
   
    setIsConfirmationOpen(true);
     window.location.reload();

    // postData("Cancelled");
  };

  const handleBookAppointmentClick2 = () => {
    SetStatus("Rescheduled");
    setConfirmationAction("Re-Schedule Appointment");
  
    setIsConfirmationOpen(true);
      window.location.reload(); 
    // postData("Rescheduled");
  };

  let AdminId = localStorage.getItem("AdminId");
  const postData = async (bookingStatus) => {
    const appointmentId = localStorage.getItem("appointmentDetailId");
    try {
      const payload = { status: bookingStatus, purposeOfVisit: pov };
      const response = await axios.patch(
        `${BASE_ASSET_URL}/appointment-details/${appointmentId}`,
        payload
      );
      const id = response.data._id;
    } catch (error) {
      console.error("Error:", error);
    }
  };

  console.log("Admin--->", AdminId);
  const getdoctor = async () => {
    try {
      const response = await axios.get(
        `${BASE_ASSET_URL}/businessinfo/admin/${AdminId}`
      );
      const data = response.data;
      console.log(data);
      setname(data.name);
      setdata({
        name: data.name || "",
        emailId: data.emailId || "",
        contactNumber: data.contactNumber || "",
        address: data.address || "",
        logo: data.logo || "",
      });
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    getdoctor();
  }, []);

  // const handleapicall = (event) => {
  //   event.preventDefault();
  //   console.log("Re-Schedule button clicked");
  //   getdoctor();
  // };

  const handleConfirmation = async (confirmed) => {
    if (confirmed) {
      await postData(status);
      setShowAppointment(true);
      setCurrentComp(false);
    }
    setIsConfirmationOpen(false);
  };
  const handleConfirmation2 = async (confirmed) => {
    if (confirmed) {
      await postData(status);
    }
    setIsConfirmationOpen(false);
    setpov("");
    alert("Message Sent Successfully");
  };
  const handlePov = (event) => {
    const value = event.target.value;
    setpov(value);
    console.log("---->>" + pov);
  };

  return (
    <>
      <DraggableDialog
        open={isConfirmationOpen}
        handleClose={() => setIsConfirmationOpen(false)}
        handleConfirmation={handleConfirmation}
        confirmationMessage={`Are you sure you want to ${confirmationAction.toLowerCase()}?`}
      />
      {CurrentComp && (
        <div className="colony">
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-md-offset-1">
                {/* <div className="colony_top">
                <div className="row">
                  <div className="col-md-6">
                    <div className="cc_wrap">
                      <div className="cc_wrap_left">
                        <img
                           src={'http://localhost:8000' + "/" + data.logo.filename}
                          className="img-responsive"
                          alt=""
                        />
                      </div>
                      <div className="cc_wrap_right">
                        <h3>{data.contactNumber} </h3>
                        <p>{data.emailId} </p>
                        <p>{data.address}</p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 colony_buttons">
                    <a href="#" className="btn btn_new2" >
                      View Detail
                    </a>
                    <a href="#" className="btn btn_new1" onClick={handleBookAppointmentClick}>
                      Cancel Request
                    </a>
                  </div>
                  <div className="clearfix" />
                </div>
                <div className="clearfix" />
              </div> */}
              </div>
              <div className="clearfix" />
            </div>
            <div className="clearfix" />
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-md-offset-1">
                <div className="confirmed">
                  <div className="row">
                    <div className="col-md-12 tick_top  zoomIn">
                      <img
                        src="images/person.png"
                        className="img-responsive center-block"
                        alt=""
                      />
                    </div>
                    <div className="col-md-12">
                      <h2>{name}</h2>
                    </div>
                    <div className="col-md-12 bookingcnf">
                      <h2>
                        <span>
                          <i
                            class="fa fa-phone"
                            style={{
                              fontSize: "16px",
                              color: "white",
                              marginRight: "2px",
                            }}
                          ></i>
                        </span>
                        +91 {data.contactNumber}
                      </h2>
                      <h2>
                        <span>
                          <i
                            class="fa fa-envelope"
                            style={{
                              fontSize: "16px",
                              color: "white",
                              marginRight: "2px",
                            }}
                          ></i>
                        </span>
                        {data.emailId}
                      </h2>
                    </div>
                    <div className="bookingcnf2">
                      <h2>
                        {" "}
                        <span>
                          <i
                            class="fa fa-map-marker"
                            style={{
                              fontSize: "16px",
                              color: "white",
                              marginRight: "2px",
                              marginTop: "5px",
                            }}
                          ></i>
                        </span>{" "}
                        {data.address}
                      </h2>
                    </div>
                    <div className="clearfix" />
                  </div>
                  <div className="clearfix" />
                  <div class="divider"></div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <a href="#" className="btn btn_new21">
                      {selectedDate}
                    </a>
                    <a href="#" className="btn btn_new21">
                      {selectedDay}
                    </a>
                    <a href="#" className="btn btn_new21">
                      {selectedSlots}
                    </a>
                  </div>
                  <div className="pov form-control">
                    <input
                      type="text"
                      value={pov}
                      id="pov"
                      placeholder="Please Share Your Purpose of Visit"
                      style={{
                        color: "white",
                        "::placeholder": { color: "white" },
                      }}
                      onChange={handlePov}
                    />

                    <div className="rounded-icon">
                      <SendRoundedIcon
                        onClick={() => handleConfirmation2(true)}
                        style={{
                          fontSize: "25px",
                          color: "white",
                          paddingLeft: 3,
                        }}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6  slideInLeft">
                      <div className="cc_wrap">
                        {/* <div className="cc_wrap_left">
                        <img
                          src="images/person.png"
                          className="img-responsive"
                          alt=""
                        />
                      </div> */}
                        {/* <div className="cc_wrap_right">
                        <h3>Doctor will be assigned </h3>
                        <p>
                          <span>1 hour</span> before the scheduled time
                        </p>
                      </div> */}
                      </div>
                  
                    </div>
                    {/* <div className="col-md-6 confirm_right  slideInRight">
                    <h3>{name}</h3>
                    <p>
                      <img
                        src="images/clock.png"
                        className="img-responsive"
                        alt=""
                      />{" "}
                    {selectedDate}<br />{" "}{selectedSlots}
                    </p>
                  </div> */}
                  
                    <div className="clearfix" />
                 
                    <div className="text-center b_a_btn">
                    <GoogleCalendarSync/>
                      <a
                        href="#"
                        className="btn btn_new2"
                        onClick={handleBookAppointmentClick2}
                      >
                        Re-Schedule Appointment
                      </a>
                      
                      <a
                        href="#"
                        className="btn btn_new22"
                        onClick={handleBookAppointmentClick}
                      >
                        Cancel Appointment
                      </a>
                    </div>
                  </div>
                  <div className="clearfix" />
                </div>
              </div>
              <div className="clearfix" />
            </div>
            <div className="clearfix" />
          </div>
        </div>
      )}
      {showAppointment && <BookYourSlots />}
    </>
  );
};

export default BooingConfirm;
