"use client";
import Verification from "./verification";
import { useState } from "react";

const FillYourInfo = ({ getnumber, cnfotp }) => {
  const [showAppointment, setShowAppointment] = useState(false);
  const [CurrentComp, setCurrentComp] = useState(true);
  const [phoneNumber, setPhoneNumber] = useState("");
  const [phoneNumberError, setPhoneNumberError] = useState("");

  const handlePhoneNumber = (event) => {
    const value = event.target.value;

    setPhoneNumber(value);

    if (!validatePhoneNumber(value)) {
      setPhoneNumberError("Please enter a valid phone number.");
    } else {
      setPhoneNumberError("");
    }
  };

  const handleBookAppointmentClick = (event) => {
    event.preventDefault();
    if (validatePhoneNumber(phoneNumber)) {
      setShowAppointment(true);
      setCurrentComp(false);

      getnumber(phoneNumber);
    } else {
      console.error("Please enter a valid phone number before booking.");
    }
  };

  const validatePhoneNumber = (phoneNumber) => {
    const phoneRegex = /^\d{10}$/;
    return phoneRegex.test(phoneNumber);
  };

  return (
    <>
      {CurrentComp && (
        <div className="congo">
          <div className="container">
            <div className="row">
              <div className="col-md-8 col-md-offset-2 congo_top">
                <img
                  src="images/congrats.png"
                  className="img-responsive center-block zoomIn"
                  alt=""
                />
                <p>
                  This is the final step to book your appointment you are almost
                  done
                </p>
              </div>
              <div className="clearfix" />
              <div className="col-md-8 col-md-offset-2">
                <div className="fill_box">
                  <h3>Fill Your Information</h3>
                  <div className="clearfix" />
                  <div className="form-group">
                    <input
                      type="number"
                      className="form-control"
                      id="phoneNumber"
                      placeholder="Enter Phone No."
                      value={phoneNumber}
                      onChange={handlePhoneNumber}
                    />
                    {phoneNumberError && (
                      <div style={{ color: "red" }}>{phoneNumberError}</div>
                    )}
                  </div>
                  <div className="clearfix" />
                  <div className="fill_buttons">
                    <a
                      className="btn btn_new2"
                      onClick={handleBookAppointmentClick}
                    >
                      Book Now
                    </a>
                    <a href="#" className="btn btn_new1">
                      Cancel
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      )}
      {showAppointment && <Verification number={phoneNumber} />}
    </>
  );
};

export default FillYourInfo;
